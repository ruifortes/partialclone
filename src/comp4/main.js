/** @jsx jsx */

import {jsx} from "@emotion/core"
export default function(props) {
let test1 = props?.someMethod() ?? "default"
	return <div
    css={{
      backgroundColor: "hotpink",
      "&:hover": {
        color: "lightgreen"
      }
    }}
  >
    This has a hotpink background.
  </div>
}
